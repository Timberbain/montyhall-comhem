import React, { Component } from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';
import SimulationForm from './components/simulationform/SimulationForm';
import SimulationSummary from './components/simulationsummary/SimulationSummary';
import SimulationTable from './components/simulationtable/SimulationTable';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="App">
        <Container>
          <Row>
            <Col className="image-header">
              <Image src="lets-make-a-deal-1.jpg" fluid />
            </Col>
          </Row>
          <Row>
            <Col>
              <SimulationForm onSimulated={({ parameters, simulations }) => this.setState({ parameters, simulations })}/>
            </Col>
          </Row>
          <Row>
            <Col>
              <SimulationSummary simulationResult={this.state.simulations} simulationParameters={this.state.parameters} />
            </Col>
          </Row>
          <Row>
            <Col>
              <div>
                If requested number of simulations exceeds 10000, the table content will be omitted.
              </div>
              <SimulationTable simulationResult={this.state.simulations} />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
