import React, { Component } from "react";
import { Badge, Table } from 'react-bootstrap';

class SimulationTable extends Component {
  badgify(door, index) {
    return (
      <Badge key={index} variant={door.v === 'car' ? 'success' : 'danger' }>
        {door.v}({door.i})
      </Badge>
    );
  }

  render() {
    const { simulationResult } = this.props;
    const simulations = (simulationResult && simulationResult.simulations) || [];
    return (
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>#</th>
            <th>Doors</th>
            <th>Initial Selection</th>
            <th>Revealed</th>
            <th>Final Selection</th>
          </tr>
        </thead>
        <tbody>
        {simulations.map((sim, i) => {
          return (<tr key={`${i}-sim-tr`}>
            <td>{i+1}</td>
            <td>{sim.doors.map((d, k) => this.badgify(d, `${i}-${k}-sim-b`))}</td>
            <td>{this.badgify(sim.selected)}</td>
            <td>{this.badgify(sim.revealed)}</td>
            <td>{this.badgify(sim.finalSelection)}</td>
          </tr>)
        })}
        </tbody>
      </Table>
    );
  }

}

export default SimulationTable;
