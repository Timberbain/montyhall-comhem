import React, { Component } from "react";
import {
  InputGroup,
  FormControl,
  Button,
  Form,
  Alert,
} from 'react-bootstrap';
import axios from 'axios';

class ServerHealth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      runningSimulation: false,
      error: '',
      simulations: 5,
      doors: 3,
      change: true,
    };
    this.runSimulations = this.runSimulations.bind(this);
  }

  async runSimulations() {
    try {
      this.setState({ runningSimulation: true, error: '' });
      console.log('Running Simulation');
      const results = await axios({
        method: 'post',
        url: '/montyhall/simulate',
        data: {
          nr: this.state.simulations,
          change: this.state.change,
          nr_of_doors: this.state.doors,
        }
      });
      this.props.onSimulated({
        parameters: this.state,
        simulations: results.data,
      });
    } catch (e) {
      console.log(e);
      if (e.response) {
        this.setState({ error: e.response.data });
      } else {
        this.setState({ error: e.message });
      }

      throw e;
    } finally {
      this.setState({ runningSimulation: false });
    }
  };

  render() {
    return (
      <div>
        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text
              id="input-simulations"
            >
              # Simulations
            </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            placeholder="Simulations"
            aria-label="Simulations"
            defaultValue={this.state.simulations}
            aria-describedby="input-simulations"
            onChange={e => this.setState({ simulations: e.target.value })}
          />
        </InputGroup>
        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text
              id="input-doors"
            >
              # Doors
            </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            placeholder="Doors"
            aria-label="Doors"
            defaultValue={this.state.doors}
            aria-describedby="input-doors"
            onChange={e => this.setState({ doors: e.target.value })}
          />
        </InputGroup>
        <InputGroup className="mb-3">
          <Form.Check
            type='checkbox'
            id="input-change-selection"
            label="Change selection after revelation"
            checked={this.state.change}
            onChange={e => this.setState({ change: e.target.checked })}
          />
        </InputGroup>
        <InputGroup className="mb-3">
          {
            this.state.runningSimulation
            ? <Button variant="secondary" disabled>Simulating</Button>
            : <Button variant="primary" onClick={this.runSimulations}>Simulate</Button>
          }
        </InputGroup>
        {
          this.state.error
          ? <Alert variant="danger">{this.state.error}</Alert>
          : <div></div>
        }

      </div>
    );
  }

}

export default ServerHealth;
