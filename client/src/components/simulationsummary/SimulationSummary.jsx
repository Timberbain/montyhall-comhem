import React, { Component } from "react";
import {
  Card,
} from 'react-bootstrap';

class SimulationView extends Component {
  render() {
    const { simulationResult, simulationParameters } = this.props;
    const result = (simulationResult && simulationResult.result) || { goat: {}, car: {} };
    const simulations = (simulationParameters && simulationParameters.simulations) || 'N/A';
    const doors = (simulationParameters && simulationParameters.doors) || 'N/A';
    const change = (simulationParameters && simulationParameters.change);

    return (
      <Card>
        <Card.Body>
          <Card.Text>
            Number of Simulations:
              <br />{simulations}<br />
            Number of Doors:
              <br />{doors}<br />
            Change after revelation:
              <br />{change === true ? 'true' : 'false'}<br />
            <br />
            Results
            <br />
            Goat: {result.goat.count} ({result.goat.share * 100}%)
            Car: {result.car.count} ({result.car.share * 100}%)
          </Card.Text>
        </Card.Body>
      </Card>
    );
  }

}

export default SimulationView;
