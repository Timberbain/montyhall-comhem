const express = require('express');

const { addToRouterInFolder } = require('../util/routes');

const router = express.Router();

addToRouterInFolder('/', router, __dirname);

module.exports = router;
