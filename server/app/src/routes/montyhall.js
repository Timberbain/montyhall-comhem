const express = require('express');

const bodyParser = require('body-parser');

const router = express.Router();

const { celebrate, Joi } = require('celebrate');

const c = require('../middlewares/controller_handler');

const { runSimulations } = require('../controllers/montyhall');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

router.post('/simulate',
  celebrate({
    body: {
      nr: Joi.number().integer().min(1).default(10),
      change: Joi.boolean().default(false),
      nr_of_doors: Joi.number().integer().min(1).default(3),
    },
  }),
  c(runSimulations, req => [
    req.body.nr,
    req.body.change,
    req.body.nr_of_doors,
  ]));

module.exports = router;
