const express = require('express');

const morgan = require('morgan');

const app = express();
const port = 3011;

const api = require('./routes');

app.use(morgan('dev'));

app.use('/', api);

// error handler
app.use((err, req, res, next) => {      // eslint-disable-line
  res.status(err.status || 500);
  res.send(err.message);
});

// eslint-disable-next-line
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
