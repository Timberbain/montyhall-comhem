const { isset, randInt } = require('../util/util');

/* Constants */
const GOAT = 'goat';
const CAR = 'car';
const MAX_SIM_DETAILS = 10000;


/* Generate a set of goats and cars */
function generateInput(nrOfDoors, nrOfCars) {
  const doors = Array.from({ length: nrOfDoors }, () => GOAT);
  const potentialPrices = Array.from(Array(nrOfDoors).keys());
  for (let i = 0; i < nrOfCars; i += 1) {
    const indexPointer = randInt(potentialPrices.length);
    const priceIndex = potentialPrices[indexPointer];
    potentialPrices.splice(indexPointer, 1);
    doors[priceIndex] = CAR;
  }
  return doors;
}

function simulate(inputDoors, changeDoor) {
  try {
    const doors = inputDoors.map((v, i) => ({ v, i }));
    const selected = doors[randInt(doors.length)];
    const couldBeRevealed = doors.filter(v => v.v === GOAT && v.i !== selected.i);
    const revealed = couldBeRevealed[randInt(couldBeRevealed.length)];
    const remainingNotSelected = doors.filter(v => v.i !== revealed.i && v.i !== selected.i);
    let newSelection;
    if (changeDoor) {
      newSelection = remainingNotSelected[randInt(remainingNotSelected.length)];
    }
    return {
      doors,
      selected,
      revealed,
      newSelection,
      finalSelection: newSelection || selected,
    };
  } catch (err) {
    throw err;
  }
}

function runSimulations(nrOfSim, changeDoor, nrOfDoors = 3, nrOfPrices = 1) {
  try {
    const results = {
      result: {
        [CAR]: { count: 0, share: 0 },
        [GOAT]: { count: 0, share: 0 },
      },
      simulations: [],
    };

    for (let i = 0; i < nrOfSim; i += 1) {
      const inputDoors = generateInput(nrOfDoors, nrOfPrices);
      const res = simulate(inputDoors, changeDoor);
      const selected = res.finalSelection;
      if (!isset(results.result[selected.v])) results.result[selected.v] = { count: 0 };
      results.result[selected.v].count += 1;

      /* Remove the list of simulations if the number of simulations are to great */
      if (nrOfSim < MAX_SIM_DETAILS + 1) {
        results.simulations.push(res);
      }
    }
    Object
      .keys(results.result)
      .map((r) => {
        results.result[r].share = results.result[r].count / nrOfSim;
        return 0;
      });

    return results;
  } catch (err) {
    throw err;
  }
}

module.exports = {
  generateInput,
  simulate,
  runSimulations,
};
