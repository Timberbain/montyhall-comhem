const fs = require('fs-extra');

module.exports = {
  addToRouterInFolder(prefix, router, path, subPath = '') {
    try {
      const files = fs.readdirSync(path);
      files
        .filter(file => file !== 'index.js')
        .map((file) => {
          const filename = file.split('.')[0];
          // eslint-disable-next-line
          router.use(`${prefix}${filename}`, require(`${path}/${filename}${subPath}`));
          return 1;
        });
    } catch (err) {
      throw err;
    }
  },
};
