module.exports = {
  isset: v => v !== null && typeof v !== 'undefined',
  randInt: num => Math.floor(Math.random() * num),
};
