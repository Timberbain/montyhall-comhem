# montyhall-comhem
En implementaion av montyhall paradoxen i react och Nodejs baserat på [crm-monty-hall-test](https://github.com/ComHem/crm-monty-hall-test-node).


### Systemkrav
[NodeJS](https://nodejs.org)

[Docker](https://www.docker.com/) (valfritt)

### Development mode
#### Start client
```
cd client
npm install
npm start
```

#### Start server

Alt 1.
```
docker-compose up
```

Alt 2.
```
cd server/app
npm install
npm start
```

#### Monty Hall
##### Server
Kör simulering genom att skicka ett POST-request till /montyhall/simulate.
API:et accepterar 3 argument i body:
```
Example body: {
  "nr": 10,
  "change": true,
  "nr_of_doors": 3
}
```

`nr` (Antal simuleringar)

`change` (Garantera att alltid byta dörr)

`nr_of_doors` (Antal dörrar)

##### Client
Öppna webbläsaren localhost:3012.
Använd formuläret för att konfigurera ditt anrop till servern.
